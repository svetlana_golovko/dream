import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.jooq.codegen.GenerationTool
import org.jooq.meta.jaxb.Configuration
import org.jooq.meta.jaxb.Database
import org.jooq.meta.jaxb.Generate
import org.jooq.meta.jaxb.Generator
import org.jooq.meta.jaxb.Jdbc

group = "com.example"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11
val jooqVersion  = "3.12.3"

val springVersion by extra("3.1.0.RELEASE")

buildscript {
	val jooqVersion  = "3.12.3"
	repositories {
		mavenCentral()
	}

	dependencies {
		classpath( group = "org.jooq", name = "jooq", version = jooqVersion)
		classpath( group = "org.jooq", name = "jooq-meta", version = jooqVersion)
		classpath( group = "org.jooq", name = "jooq-codegen", version = jooqVersion)
		classpath( group = "com.h2database", name = "h2", version = "1.4.200")
	}
}

plugins {
	id("org.springframework.boot") version "2.5.4"
	id("io.spring.dependency-management") version "1.0.11.RELEASE"
	kotlin("jvm") version "1.5.30"
	kotlin("plugin.spring") version "1.5.21"
	id("org.flywaydb.flyway") version "6.2.3"
}


repositories {
	mavenLocal()
	mavenCentral()
}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-data-r2dbc")
	implementation("org.springframework.boot:spring-boot-starter-jooq")
	implementation("org.springframework.boot:spring-boot-starter-webflux")
	implementation("org.springframework.boot:spring-boot-starter-actuator")
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
	implementation("io.projectreactor.kotlin:reactor-kotlin-extensions")
	implementation("org.flywaydb:flyway-core")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor")
	implementation("org.springframework:spring-jdbc")
	//runtimeOnly("com.h2database:h2")
	runtimeOnly("io.r2dbc:r2dbc-h2")
	testImplementation("org.springframework.boot:spring-boot-starter-test")
	testImplementation("io.projectreactor:reactor-test")
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = JavaVersion.VERSION_11.toString()
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}
tasks.register("generateJooq") {
	doLast {
		val init = "src/main/resources/schema.sql"
		GenerationTool.generate(Configuration()
				.withJdbc(Jdbc()
						.withDriver("org.h2.Driver")
						.withUrl("jdbc:h2:mem:jooq;INIT=RUNSCRIPT FROM '$init'")
						.withUser("sa")
						.withPassword(""))
				.withGenerator(Generator()
						.withDatabase(Database())
						.withGenerate(Generate()
								.withPojos(true)
								.withDaos(true))
						.withTarget(org.jooq.meta.jaxb.Target()
								.withPackageName("com.example.jooq")
								.withDirectory("src/main/java"))))
	}
}
