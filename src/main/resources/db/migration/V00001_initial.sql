CREATE TABLE product (
                         id int AUTO_INCREMENT PRIMARY KEY,
                         name VARCHAR(250) NOT NULL,
                         price double NOT NULL,
                         description VARCHAR(250) DEFAULT NULL,
                         realization_date date NOT NULL
);