DROP TABLE IF EXISTS PRODUCT;
CREATE TABLE PRODUCT (
                         ID int AUTO_INCREMENT PRIMARY KEY,
                         name VARCHAR(250) NOT NULL,
                         price double NOT NULL,
                         description VARCHAR(250) DEFAULT NULL,
                         realization_date date NOT NULL,
                         PRIMARY KEY (ID)
);