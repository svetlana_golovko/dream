/*
 * This file is generated by jOOQ.
 */
package com.example.jooq.public_.tables.daos;


import com.example.jooq.public_.tables.Product;
import com.example.jooq.public_.tables.records.ProductRecord;

import java.sql.Date;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Configuration;
import org.jooq.impl.DAOImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class ProductDao extends DAOImpl<ProductRecord, com.example.jooq.public_.tables.pojos.Product, Integer> {

    /**
     * Create a new ProductDao without any configuration
     */
    public ProductDao() {
        super(Product.PRODUCT, com.example.jooq.public_.tables.pojos.Product.class);
    }

    /**
     * Create a new ProductDao with an attached configuration
     */
    public ProductDao(Configuration configuration) {
        super(Product.PRODUCT, com.example.jooq.public_.tables.pojos.Product.class, configuration);
    }

    @Override
    public Integer getId(com.example.jooq.public_.tables.pojos.Product object) {
        return object.getId();
    }

    /**
     * Fetch records that have <code>ID BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<com.example.jooq.public_.tables.pojos.Product> fetchRangeOfId(Integer lowerInclusive, Integer upperInclusive) {
        return fetchRange(Product.PRODUCT.ID, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>ID IN (values)</code>
     */
    public List<com.example.jooq.public_.tables.pojos.Product> fetchById(Integer... values) {
        return fetch(Product.PRODUCT.ID, values);
    }

    /**
     * Fetch a unique record that has <code>ID = value</code>
     */
    public com.example.jooq.public_.tables.pojos.Product fetchOneById(Integer value) {
        return fetchOne(Product.PRODUCT.ID, value);
    }

    /**
     * Fetch records that have <code>NAME BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<com.example.jooq.public_.tables.pojos.Product> fetchRangeOfName(String lowerInclusive, String upperInclusive) {
        return fetchRange(Product.PRODUCT.NAME, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>NAME IN (values)</code>
     */
    public List<com.example.jooq.public_.tables.pojos.Product> fetchByName(String... values) {
        return fetch(Product.PRODUCT.NAME, values);
    }

    /**
     * Fetch records that have <code>PRICE BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<com.example.jooq.public_.tables.pojos.Product> fetchRangeOfPrice(Double lowerInclusive, Double upperInclusive) {
        return fetchRange(Product.PRODUCT.PRICE, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>PRICE IN (values)</code>
     */
    public List<com.example.jooq.public_.tables.pojos.Product> fetchByPrice(Double... values) {
        return fetch(Product.PRODUCT.PRICE, values);
    }

    /**
     * Fetch records that have <code>DESCRIPTION BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<com.example.jooq.public_.tables.pojos.Product> fetchRangeOfDescription(String lowerInclusive, String upperInclusive) {
        return fetchRange(Product.PRODUCT.DESCRIPTION, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>DESCRIPTION IN (values)</code>
     */
    public List<com.example.jooq.public_.tables.pojos.Product> fetchByDescription(String... values) {
        return fetch(Product.PRODUCT.DESCRIPTION, values);
    }

    /**
     * Fetch records that have <code>REALIZATION_DATE BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<com.example.jooq.public_.tables.pojos.Product> fetchRangeOfRealizationDate(Date lowerInclusive, Date upperInclusive) {
        return fetchRange(Product.PRODUCT.REALIZATION_DATE, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>REALIZATION_DATE IN (values)</code>
     */
    public List<com.example.jooq.public_.tables.pojos.Product> fetchByRealizationDate(Date... values) {
        return fetch(Product.PRODUCT.REALIZATION_DATE, values);
    }
}
