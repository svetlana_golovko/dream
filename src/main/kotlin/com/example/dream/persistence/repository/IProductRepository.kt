package com.example.dream.persistence.repository

import com.example.jooq.public_.tables.records.ProductRecord
import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.repository.PagingAndSortingRepository
import java.time.LocalDate
import java.util.*

abstract class IProductRepository : PagingAndSortingRepository<ProductRecord, Long>  {
    abstract fun findByName(name: String?): Optional<ProductRecord?>?

    @Query("select t from Product t where t.name like %?1%")
    abstract fun findByNameMatches(name: String?): List<ProductRecord?>?

    abstract fun findByRealizationDateBetween(start: LocalDate?, end: LocalDate?): List<ProductRecord?>?
}