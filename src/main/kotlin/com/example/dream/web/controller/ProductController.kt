package com.example.dream.web.controller

import com.example.dream.service.IProductService
import com.example.jooq.public_.tables.records.ProductRecord
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException
import java.util.*
import kotlin.Long
import kotlin.String

@RestController
@RequestMapping(value = ["/products"])
class ProductController {
    private var productService: IProductService? = null

    fun ProductController(productService: IProductService?) {
        this.productService = productService
    }

    @GetMapping(path = ["/{id}"])
    fun findOne(@PathVariable id: Long?): ProductRecord? {
        return productService?.findById(id)?.orElseThrow { ResponseStatusException(HttpStatus.NOT_FOUND) }
    }

    @PostMapping
    fun create(@RequestBody product: ProductRecord?): Int? {
        val savedProduct: ProductRecord = productService?.save(product)
        return savedProduct.id
    }

    @PutMapping(path = ["/{id}"])
    fun update(@PathVariable id: Int?, @RequestBody product: ProductRecord): Int? {
        productService?.findById(id)?.orElseThrow { ResponseStatusException(HttpStatus.NOT_FOUND) }
        product.id = id
        val savedProduct: ProductRecord = productService.save(product)
        return savedProduct.id
    }

    @DeleteMapping(path = ["/{id}"])
    fun delete(@PathVariable id: Long?) {
        productService?.delete(id)
    }

    @GetMapping(path = [""])
    fun findAll(@RequestParam sort: Optional<String>, @RequestParam forward: Optional<String?>): List<ProductRecord?>? {
        val asc = forward?.equals("Y")
        return productService?.findAllAndSortBy(sort.orElseGet { "name" }, asc)
    }
}