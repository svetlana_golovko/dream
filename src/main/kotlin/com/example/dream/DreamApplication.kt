package com.example.dream

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class DreamApplication

fun main(args: Array<String>) {
	runApplication<DreamApplication>(*args)
}
