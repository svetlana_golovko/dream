package com.example.dream.service

import java.util.*
import com.example.jooq.public_.tables.records.ProductRecord

interface IProductService {
    fun findById(id: Long?): Optional<ProductRecord?>?

    fun save(product: ProductRecord?): ProductRecord?

    fun delete(id: Long?)

    fun findAllAndSortBy(fieldName: String?, forward: Boolean?): List<ProductRecord?>?
}