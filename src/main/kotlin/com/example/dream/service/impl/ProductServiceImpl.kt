package com.example.dream.service.impl

import com.example.dream.persistence.repository.IProductRepository
import com.example.dream.service.IProductService
import com.example.jooq.public_.tables.Product
import com.example.jooq.public_.tables.records.ProductRecord
import org.springframework.data.domain.Sort
import java.util.*

class ProductServiceImpl: IProductService {
    private var productRepository: IProductRepository? = null

    fun ProductServiceImpl(projectRepository: IProductRepository?) {
        productRepository = projectRepository
    }

    override fun findById(id: Long?): Optional<ProductRecord?>? {
        return productRepository!!.findById(id!!)
    }

    override fun save(product: ProductRecord?): ProductRecord? {
        return productRepository!!.save(product!!)
    }

    override fun delete(id: Long?) {
        productRepository!!.deleteById(id!!)
    }

    override fun findAllAndSortBy(fieldName: String?, forward: Boolean?): List<ProductRecord?>? {
        var sort: Sort? = null
        if (fieldName != null) sort = forward?.let {  Sort.by(Sort.Order.asc(fieldName)) } ?: Sort.by(Sort.Order.desc(fieldName))
        return productRepository!!.findAll(sort!!) as List<ProductRecord?>
    }


}